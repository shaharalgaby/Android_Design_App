package com.example.android.android_me.ui;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.android.android_me.R;
import com.example.android.android_me.data.AndroidImageAssets;

import java.util.ArrayList;
import java.util.List;

public class LegsPartFragment extends Fragment {

    private static final String TAG = "LegsPartFragment";
    public static final String IMAGE_ID_LIST = "image_ids";
    public static final String LIST_INDEX = "list_index";

    private List<Integer> imageIds;
    private int listIndex;

    public LegsPartFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(savedInstanceState!=null){
            imageIds = savedInstanceState.getIntegerArrayList(IMAGE_ID_LIST);
            listIndex = savedInstanceState.getInt(LIST_INDEX);
        }

        View view = inflater.inflate(R.layout.fragment_legs_part,container,false);

        final ImageView imageView = (ImageView) view.findViewById(R.id.legs_part_image_view);
        imageView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(listIndex<imageIds.size()-1){
                    listIndex++;
                }else{
                    listIndex = 0;
                }
                imageView.setImageResource(imageIds.get(listIndex));
            }
        });

        if(imageIds!=null)
            imageView.setImageResource(imageIds.get(listIndex));
        else
            Log.v(TAG,"This fragment has a null list of image ids");

        return view;
    }

    public void setImageIds(List<Integer> imageIds) {
        this.imageIds = imageIds;
    }

    public void setListIndex(int listIndex) {
        this.listIndex = listIndex;
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        state.putIntegerArrayList(IMAGE_ID_LIST,(ArrayList<Integer>)imageIds);
        state.putInt(LIST_INDEX,listIndex);
    }
}
